FROM node
COPY . /usr/local/src/app
ENV REDIS_HOST redis
WORKDIR /usr/local/src/app
RUN npm install --production
EXPOSE 8081
CMD ["/usr/local/bin/node", "/usr/local/src/app/server.js"]